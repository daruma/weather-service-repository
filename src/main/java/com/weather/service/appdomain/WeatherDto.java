package com.weather.service.appdomain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

@Data
public class WeatherDto {
    private List<Weather> weather;
    private String stations;
    private Main main;
    private int visibility;
    private Wind wind;
    private Clouds clouds;
    private long dt;
    private Sys sys;
    private long id;
    private String name;
    private int cod;

    @JsonIgnore
    private String base;
    @JsonIgnore
    private Coord coord;
}
