package com.weather.service.appdomain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Wind {
    private float speed;

    @JsonProperty("deg")
    private float degree;
}
