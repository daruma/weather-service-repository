package com.weather.service.appdomain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Weather {
    private String description;

    @JsonProperty("main")
    private String state;

    @JsonIgnore
    private String icon;

    @JsonIgnore
    private int id;
}
