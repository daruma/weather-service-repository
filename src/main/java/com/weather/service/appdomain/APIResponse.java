package com.weather.service.appdomain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class APIResponse {
    private final String response;
    private final int statusCode;
}
