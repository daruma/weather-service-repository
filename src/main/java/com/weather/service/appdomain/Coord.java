package com.weather.service.appdomain;

import lombok.Data;

@Data
public class Coord {
    private double lat;
    private double lon;
}
