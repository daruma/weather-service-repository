package com.weather.service.appdomain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class Sys {
    private long sunrise;
    private long sunset;

    @JsonIgnore
    private int type;
    @JsonIgnore
    private int id;
    @JsonIgnore
    private float message;
    @JsonIgnore
    private String country;
}
