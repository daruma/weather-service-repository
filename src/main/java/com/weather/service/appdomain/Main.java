package com.weather.service.appdomain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Main {
    @JsonProperty("temp")
    private float temperature;
    private float pressure;
    private int humidity;

    @JsonProperty("sea_level")
    private float seaLevel;

    @JsonProperty("grnd_level")
    private float groundLevel;

    @JsonIgnore
    private float temp_min;
    @JsonIgnore
    private float temp_max;
}
