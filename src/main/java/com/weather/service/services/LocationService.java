package com.weather.service.services;

import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.weather.service.api.DatabaseReaderWrapper;
import com.weather.service.config.CachingConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.weather.service.exception.WeatherServiceException;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Service
@Slf4j
public class LocationService {

    private final DatabaseReaderWrapper databaseReaderWrapper;

    @Autowired
    public LocationService(DatabaseReaderWrapper databaseReaderWrapper) {
        this.databaseReaderWrapper = databaseReaderWrapper;
    }

    @Cacheable(CachingConfiguration.LOCATIONS_CACHE)
    public CityResponse getCity(String ipAddress) throws WeatherServiceException {
        try {

            return databaseReaderWrapper.city(getInetAddress(ipAddress));

        } catch (GeoIp2Exception e) {
            throw new WeatherServiceException("City not found by ip " + ipAddress, e);
        } catch (UnknownHostException e) {
            throw new WeatherServiceException("Failed to retrieve InetAddress for ip " + ipAddress, e);
        } catch (IOException e) {
            throw new WeatherServiceException("Failed to read database file.", e);
        }
    }

    private InetAddress getInetAddress(String ipAddress) throws UnknownHostException {
        return InetAddress.getByName(ipAddress);

    }

}
