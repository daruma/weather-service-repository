package com.weather.service.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.weather.service.api.OpenWeatherMapProvider;
import com.weather.service.appdomain.APIResponse;
import com.weather.service.appdomain.WeatherDto;
import com.weather.service.config.CachingConfiguration;
import com.weather.service.exception.WeatherServiceException;
import com.weather.service.models.Location;
import com.weather.service.models.WeatherModel;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class WeatherService {

    private final OpenWeatherMapProvider openWeatherMapProvider;
    private final ObjectMapper objectMapper;
    private final ModelMapper modelMapper;

    private static final int HTTP_STATUS_OK = 200;

    @Autowired
    public WeatherService(OpenWeatherMapProvider openWeatherMapProvider,
                          ObjectMapper objectMapper,
                          ModelMapper modelMapper) {
        this.openWeatherMapProvider = openWeatherMapProvider;
        this.objectMapper = objectMapper;
        this.modelMapper = modelMapper;
    }

    @Cacheable(CachingConfiguration.WEATHER_MAP_CACHE)
    public WeatherModel getWeatherMap(Location location) throws WeatherServiceException {
        APIResponse response = openWeatherMapProvider.getWeatherMap(location.getLocationLatitude(), location.getLocationLongitude());

        try {
            if (response.getStatusCode() == HTTP_STATUS_OK) {
                WeatherDto weatherDto = objectMapper.readValue(response.getResponse(), WeatherDto.class);
                return modelMapper.map(weatherDto, WeatherModel.class);
            }

            return new WeatherModel("OpenWeatherMap service is unavailable. Please try again later.");

        } catch (IOException e) {
            throw new WeatherServiceException("Failed to parse weather map json.", e);
        }
    }
}
