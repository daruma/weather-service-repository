package com.weather.service.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Location {
    private String cityName;
    private String countryName;
    private double locationLatitude;
    private double locationLongitude;
}
