package com.weather.service.models;

import com.weather.service.appdomain.Main;
import com.weather.service.appdomain.Sys;
import com.weather.service.appdomain.Weather;
import com.weather.service.appdomain.Wind;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@Data
public class WeatherModel {

    List<Weather> weather;
    private Main main;
    private Wind wind;
    private Sys sys;

    private Location location;

    private String errorMessage;

    public WeatherModel(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}