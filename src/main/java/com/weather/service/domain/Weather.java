package com.weather.service.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
/**
 *    A TEMPLATE
 *
 * */

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "WEATHER")
public class Weather {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Double amount;

    @Column(name = "term_months")
    private int termMonths;

    @Column(name = "ip_id")
    private Long ipId;

    private Boolean exhausted;

    @Column(name = "agreement_date")
    private Date agreementDate;
}
