package com.weather.service.controller;

import com.maxmind.geoip2.model.CityResponse;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.weather.service.exception.WeatherServiceException;
import com.weather.service.models.Location;
import com.weather.service.models.WeatherModel;
import com.weather.service.services.LocationService;
import com.weather.service.services.WeatherService;

@RestController
@RequestMapping(value = "/api/v1")
@Slf4j
public class WeatherController {

    @Autowired
    private WeatherService weatherService;
    @Autowired
    private LocationService translationService;
    @Autowired
    private ModelMapper modelMapper;

    @RequestMapping(value = "/weather/{ipAddress}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public WeatherModel getCurrentWeather(@PathVariable String ipAddress) {
        CityResponse cityResponse;
        Location location = null;
        try {
            cityResponse = translationService.getCity(ipAddress);
            location = modelMapper.map(cityResponse, Location.class);
            WeatherModel model = weatherService.getWeatherMap(location);
            model.setLocation(location);
            return model;
        } catch (WeatherServiceException e) {
            log.error("Failed to get weather forecast!", e);
        }
        return new WeatherModel("Failed to get weather forecast for " + location.getCityName() + "!");

    }
}
