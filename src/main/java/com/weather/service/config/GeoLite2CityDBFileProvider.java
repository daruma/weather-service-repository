package com.weather.service.config;

import lombok.extern.slf4j.Slf4j;

import javax.print.attribute.URISyntax;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@Slf4j
public class GeoLite2CityDBFileProvider {

    public File extractGeoLite2CityDBArchive() {
        URL database = getClass().getClassLoader().getResource("db/geoLite2City/GeoLite2-City.mmdb");
        if (database == null) {
            throw new IllegalStateException("Failed to load db file from resources.");
        }
        try {
            return Paths.get(database.toURI()).toFile();
        } catch (URISyntaxException e) {
            log.error("Error loading GeoLite2City database. Exiting.");
            throw new IllegalStateException("Error loading GeoLite2City database. Exiting.");
        }
    }
}
