package com.weather.service.config;

import com.maxmind.geoip2.DatabaseReader;
import com.weather.service.exception.WeatherServiceException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;

@Configuration
public class DatabaseReaderConfiguration {

    @Bean
    public DatabaseReader databaseReader() throws WeatherServiceException {
        try {
            File geoLite2CityFile = new GeoLite2CityDBFileProvider().extractGeoLite2CityDBArchive();
            return new DatabaseReader.Builder(geoLite2CityFile).build();
        } catch (IOException e) {
            throw new WeatherServiceException("Failed to create DatabaseReader instance.", e);
        }
    }

}
