package com.weather.service.api;

import org.assertj.core.util.VisibleForTesting;
import org.springframework.stereotype.Component;
import com.weather.service.appdomain.APIResponse;
import com.weather.service.exception.WeatherServiceException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Optional;

import static org.apache.logging.log4j.util.Strings.EMPTY;

@Component
public class OpenWeatherMapProvider {

    public APIResponse getWeatherMap(double latitude, double longitude) throws WeatherServiceException {
        HttpURLConnection connection;

        try {
            String url = "http://api.openweathermap.org/data/2.5/weather?" +
                    latitude(latitude) + "&" +
                    longitude(longitude) +
                    "&APPID=4aee5cbcf4a7eb301721e5fa21343e98" +
                    "&units=metric";
            connection = (HttpURLConnection) (new URL(url)).openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.connect();
            StringBuilder builder;

            try (InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());
                 BufferedReader bufferedReader = new BufferedReader(streamReader)) {
                builder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    builder.append(line).append(System.lineSeparator());
                }
            }

            connection.disconnect();

            return new APIResponse(
                    Optional.of(builder).map(StringBuilder::toString).orElse(EMPTY),
                    connection.getResponseCode());

        } catch (IOException e) {
            throw new WeatherServiceException("Failed to get weather data.", e);
        }
    }

    @VisibleForTesting
    String latitude(double latitude) {
        return "lat=" + latitude;
    }

    @VisibleForTesting
    String longitude(double longitude) {
        return "lon=" + longitude;
    }

}
