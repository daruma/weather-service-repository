package com.weather.service.api;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.InetAddress;

@Component
public class DatabaseReaderWrapper {

    private final DatabaseReader databaseReader;

    @Autowired
    public DatabaseReaderWrapper(DatabaseReader databaseReader) {
        this.databaseReader = databaseReader;
    }

    public CityResponse city(InetAddress ipAddress) throws IOException, GeoIp2Exception {
        return databaseReader.city(ipAddress);
    }

}
