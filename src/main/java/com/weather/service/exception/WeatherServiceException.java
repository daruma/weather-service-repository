package com.weather.service.exception;

public class WeatherServiceException extends Exception {

    public WeatherServiceException(String message){

        super(message);
    }

    public WeatherServiceException(String message, Throwable throwable){
        super(message, throwable);
    }
}
