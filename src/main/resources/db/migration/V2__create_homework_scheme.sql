---- TEMPLATE ----

-- noinspection SqlNoDataSourceInspectionForFile
drop table user_ip_address;
drop table weather;
drop table extension;

create table user_ip_address(
id int auto_increment primary key,
ip varchar(20) not null
);

create table weather(
id int auto_increment primary key,
amount decimal(5,2) not null,
term_months int not null,
rate_type char(2) default 'bs',
exhausted bit default 0,
agreement_date DATE default current_timestamp,
ip_id int not null
);