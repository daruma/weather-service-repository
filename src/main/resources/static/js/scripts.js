const API_URL = '/weather-service/api/v1/weather/';
const ipAddressInput = document.getElementById("ip_address");
const WEATHER_PARAGRAPH_ID = 'weatherDiv';
const weatherParagraph = document.getElementById(WEATHER_PARAGRAPH_ID);

function fetchWeather() {

    return fetch(API_URL + ipAddressInput.value, {credentials: 'same-origin', mode: 'same-origin', method: "get"})
        .then(resp => {
            if (resp.status === 200) {
                return resp.json()
            } else {
                console.log("Status: " + resp.status);
                return Promise.reject("server")
            }
        })
        .then(json => {
            removePreviousData();
            if (json['errorMessage']) {
                alert(json['errorMessage']);
                return;
            }
            let header = document.createElement('h3');
            header.innerText = 'Weather Data';
            weatherParagraph.appendChild(header);
            weatherParagraph.appendChild(setResponse(json));
        })
        .catch(err => {
            if (err === "server") return;
            alert(err);
        });
}

document.getElementById('submit_ip_address').addEventListener("click", fetchWeather);

function populate(obj, table) {
    Object.keys(obj).forEach(function (key) {
        let tr = document.createElement('tr');
        let keyTd = document.createElement('td');
        let valTd = document.createElement('td');
        keyTd.innerText = key;
        valTd.innerText = obj[key];
        tr.appendChild(keyTd)
        tr.appendChild(valTd)
        table.appendChild(tr);
    });
}

function setResponse(json) {
    let table = document.createElement('table');
    populate(json['location'], table);
    json['weather'].forEach(function (item, key) {
        populate(item, table);
    });
    populate(json['wind'], table);
    populate(json['sys'], table);
    populate(json['main'], table);
    return table;
}

function removePreviousData() {
    while (weatherParagraph.firstChild) {
        weatherParagraph.removeChild(weatherParagraph.firstChild);
    }
}

