package com.weather.service.api;

import org.junit.Test;
import com.weather.service.appdomain.APIResponse;
import com.weather.service.exception.WeatherServiceException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class OpenWeatherMapProviderTest {

    @Test
    public void getWeatherMap() throws WeatherServiceException {
        APIResponse response = new OpenWeatherMapProvider().getWeatherMap(28.9594, 118.8686);
        assertThat(response.getResponse().contains("Quzhou"), is(true));
        assertThat(response.getResponse().contains("humidity"), is(true));
        assertThat(response.getResponse().contains("weather"), is(true));
        assertThat(response.getResponse().contains("sea_level"), is(true));
        assertThat(response.getResponse().contains("sunrise"), is(true));
        assertThat(response.getStatusCode(), is(200));
    }

    @Test
    public void testLatitude(){
        assertThat(new OpenWeatherMapProvider().latitude(23.345), is("lat=23.345"));
        assertThat(new OpenWeatherMapProvider().latitude(3.45), is("lat=3.45"));
    }

    @Test
    public void testLongitude(){
        assertThat(new OpenWeatherMapProvider().longitude(23.345), is("lon=23.345"));
        assertThat(new OpenWeatherMapProvider().longitude(3.45), is("lon=3.45"));
    }
}