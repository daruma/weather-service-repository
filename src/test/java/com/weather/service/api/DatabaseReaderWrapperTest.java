package com.weather.service.api;

import com.maxmind.geoip2.exception.AddressNotFoundException;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.weather.service.config.DatabaseReaderConfiguration;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseReaderWrapper.class, DatabaseReaderConfiguration.class})
public class DatabaseReaderWrapperTest {
    @Autowired
    private DatabaseReaderWrapper readerWrapper;

    @Test
    public void testCity() throws IOException, GeoIp2Exception {
        InetAddress inetAddress = InetAddress.getByName("080.189.144.232");
        CityResponse response = readerWrapper.city(inetAddress);
        assertThat(response.getCity().getName(), is("Sheffield"));
        assertThat(response.getCountry().getName(), is("United Kingdom"));
    }

    @Test(expected = AddressNotFoundException.class)
    public void testCity_addressNotFoundException() throws IOException, GeoIp2Exception{
        InetAddress inetAddress = InetAddress.getByName("127.0.0.1");
        readerWrapper.city(inetAddress);
    }

    @Test(expected = UnknownHostException.class)
    public void testCity_unknownHostException() throws IOException, GeoIp2Exception{
        InetAddress inetAddress = InetAddress.getByName("1289.1");
        readerWrapper.city(inetAddress);
    }
}