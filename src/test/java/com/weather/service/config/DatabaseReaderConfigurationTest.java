package com.weather.service.config;

import org.junit.Test;
import org.powermock.reflect.Whitebox;
import org.springframework.core.io.DefaultResourceLoader;
import com.weather.service.exception.WeatherServiceException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DatabaseReaderConfigurationTest {

    @Test
    public void testDatabaseReader() throws WeatherServiceException {
        DatabaseReaderConfiguration databaseReaderConfiguration = new DatabaseReaderConfiguration();
        assertThat(databaseReaderConfiguration.databaseReader().getMetadata().getDatabaseType(), is("GeoLite2-City"));
    }
}