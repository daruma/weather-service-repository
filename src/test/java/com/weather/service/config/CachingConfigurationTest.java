package com.weather.service.config;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.springframework.cache.CacheManager;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class CachingConfigurationTest {

    @Test
    public void testCacheManager() {
        CacheManager cacheManager = new CachingConfiguration().cacheManager();

        assertThat(cacheManager, notNullValue());
        assertThat(cacheManager.getCacheNames(), CoreMatchers.hasItems(CachingConfiguration.WEATHER_MAP_CACHE, CachingConfiguration.LOCATIONS_CACHE));
    }
}