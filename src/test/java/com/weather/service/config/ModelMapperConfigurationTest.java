package com.weather.service.config;

import org.junit.Test;
import org.modelmapper.convention.MatchingStrategies;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ModelMapperConfigurationTest {

    @Test
    public void modelMapper() {
        assertThat(new ModelMapperConfiguration().modelMapper().getConfiguration().getMatchingStrategy(), is(MatchingStrategies.STANDARD));
    }
}