package com.weather.service.services;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import com.weather.service.api.DatabaseReaderWrapper;
import com.weather.service.exception.WeatherServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.weather.service.config.DatabaseReaderConfiguration;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseReaderConfiguration.class})
public class LocationServiceTest {

    @Autowired
    private DatabaseReader databaseReader;
    private LocationService locationService;

    @Before
    public void setUp(){
        locationService = new LocationService(new DatabaseReaderWrapper(databaseReader));
    }

    @Test
    public void testGetCity_Quzhou() throws WeatherServiceException {
        CityResponse actualResponse = locationService.getCity("125.105.180.252");

        assertThat(actualResponse.getCity().getName(), is("Quzhou"));
        assertThat(actualResponse.getCountry().getName(), is("China"));
        assertThat(actualResponse.getLocation().getLatitude(), is(28.9594));
        assertThat(actualResponse.getLocation().getLongitude(), is(118.8686));
    }

    @Test
    public void testGetCity_Sheffield() throws WeatherServiceException {
        CityResponse response = locationService.getCity("080.189.144.232");

        assertThat(response.getCity().getName(), is("Sheffield"));
        assertThat(response.getCountry().getName(), is("United Kingdom"));
        assertThat(response.getLocation().getLatitude(), is(53.3269));
        assertThat(response.getLocation().getLongitude(), is(-1.5403));
    }

    @Test(expected = WeatherServiceException.class)
    public void testGetCity_exception() throws WeatherServiceException {
        CityResponse response = locationService.getCity("");
    }
}