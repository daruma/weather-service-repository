package com.weather.service.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.weather.service.api.OpenWeatherMapProvider;
import com.weather.service.models.WeatherModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.weather.service.appdomain.APIResponse;
import com.weather.service.config.ModelMapperConfiguration;
import com.weather.service.exception.WeatherServiceException;
import com.weather.service.models.Location;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ModelMapperConfiguration.class, ObjectMapper.class})
public class WeatherServiceTest {

    @Mock
    private OpenWeatherMapProvider openWeatherMapProvider;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private ModelMapper modelMapper;

    private WeatherService weatherService;

    @Before
    public void setUp() {
        weatherService = new WeatherService(openWeatherMapProvider, objectMapper, modelMapper);
    }

    @Test
    public void testGetWeatherMap() throws WeatherServiceException {
        String weatherData = "{\"coord\":{\"lon\":118.87,\"lat\":28.96},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}]," +
                "\"base\":\"stations\",\"main\":{\"temp\":11.37,\"pressure\":1016.94,\"humidity\":77,\"temp_min\":11.37,\"temp_max\":11.37,\"sea_level\":1016.94,\"grnd_level\":994.71}," +
                "\"wind\":{\"speed\":2.99,\"deg\":54.788},\"clouds\":{\"all\":0},\"dt\":1557177545," +
                "\"sys\":{\"message\":0.0068,\"country\":\"CN\",\"sunrise\":1557177539,\"sunset\":1557225793}," +
                "\"id\":1797264,\"name\":\"Quzhou\",\"cod\":200}";
        Location location = new Location("Quzhou", "China", 28.9594, 118.8686);
        when(openWeatherMapProvider.getWeatherMap(location.getLocationLatitude(),location.getLocationLongitude())).thenReturn(new APIResponse(weatherData,200));
        WeatherModel model = weatherService.getWeatherMap(location);

        assertThat(model, notNullValue());
        assertThat(model.getWeather().get(0).getState(), is("Clear"));
        assertThat(model.getWeather().get(0).getDescription(), is("clear sky"));
        assertThat(model.getSys().getSunrise(), is(1557177539L));
        assertThat(model.getSys().getSunset(), is(1557225793L));
        assertThat(model.getMain().getGroundLevel(), is(994.71F));
        assertThat(model.getMain().getSeaLevel(), is(1016.94F));
        assertThat(model.getMain().getHumidity(), is(77));
        assertThat(model.getMain().getPressure(), is(1016.94F));
        assertThat(model.getMain().getTemperature(), is(11.37F));
        assertThat(model.getWind().getSpeed(), is(2.99F));
        assertThat(model.getWind().getDegree(), is(54.788F));
    }

    @Test(expected = WeatherServiceException.class)
    public void testGetWeatherMap_exception() throws WeatherServiceException {
        Location location = new Location("Quzhou", "China", 28.9594, 118.8686);
        when(openWeatherMapProvider.getWeatherMap(location.getLocationLatitude(),location.getLocationLongitude())).thenReturn(new APIResponse("weatherData",200));
        WeatherModel model = weatherService.getWeatherMap(location);
    }
}