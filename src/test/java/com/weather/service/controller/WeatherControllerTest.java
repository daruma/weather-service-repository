package com.weather.service.controller;

import com.weather.service.WeatherServiceApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WeatherServiceApplication.class)
@WebAppConfiguration
public class WeatherControllerTest {
    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetCurrentWeather() throws Exception {
        String url = "/api/v1/weather/080.189.144.232";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(url).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertThat(content.contains("Sheffield"), is(true));
        assertThat(content.contains("description"), is(true));
        assertThat(content.contains("weather"), is(true));
        assertThat(content.contains("humidity"), is(true));
        assertThat(content.contains("sea_level"), is(true));
        assertThat(content.contains("sunrise"), is(true));
        assertThat(content.contains("sunset"), is(true));
        assertThat(content.contains("locationLongitude"), is(true));
    }
}