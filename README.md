#Resume

##What is done

* Spring Boot used in order to avoid complex environment configuration and simplify app running;
* An application providing with weather map by ip address created;
* Tests written on all the logic;
* DataSource, Hibernate and TransactionManager configuration class created as a template for planed functionality;
* Caching using Spring CacheManager.

##What is not done

###Unfortunately due to a number of factors (work, family) the following requirements are not fulfilled:

* DB schema creation;
* Weather and location cache storing in DB;
* Historical analysis;
* DB versioning.
 
##Possible improvements for what is done

* UI improving (look, design);
* Data presenting (data formatting);
* Icons reflecting weather type;
* Additional UI controls, graphical data, Google maps.

After cloning a project run WeatherServiceApplication.java and follow http://localhost:8080/weather-service/index.html 
in a browser.
If you use IE the address may look different use 127.0.0.1 instead of localhost (if I am not wrong).  